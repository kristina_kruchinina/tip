package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private Button transfer;

    @FXML
    private Button change;

    @FXML
    private Label moneyRub;

    @FXML
    private Label tipRub;

    @FXML
    private TextField inputCheckAmount;

    @FXML
    private TextField rate;

    @FXML
    private RadioButton button0;

    @FXML
    private ToggleGroup buttons;

    @FXML
    private RadioButton button5;

    @FXML
    private RadioButton button3;

    @FXML
    private RadioButton button7;

    @FXML
    private RadioButton button10;

    @FXML
    private RadioButton button15;

    @FXML
    private TextField tipSize;

    @FXML
    private ComboBox<String> comboBoxCurrency;

    @FXML
    private Label lblRub;

    private ObservableList<String> currency;

    @FXML
    void initialize() {
        List<String> list = null;
        try {
            list = Files.readAllLines(Paths.get("D:\\work\\tip\\Currency.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        currency = FXCollections.observableArrayList(list);
        comboBoxCurrency.setItems(currency);
        comboBoxCurrency.setValue(list.get(0));
        lblRub.setText(comboBoxCurrency.getValue());
        comboBoxCurrency.setOnAction(event -> lblRub.setText(comboBoxCurrency.getValue()));
    }

    /**
     * Метод проверки ввода
     * корректных данных
     */
    public void stringCheck() {
        if (inputCheckAmount.getText().isEmpty()) {
            inputCheckAmount.setText("Пустое поле!");
        } else {
            if (!inputCheckAmount.getText().trim().matches("((\\d+\\.\\d*)|(\\d+))")) {
                inputCheckAmount.setText("Некорректный ввод!");
            }
        }
    }

    /**
     * Метод для подсчёта чаевых
     *
     * @param actionEvent событие
     */
    public void tipCounting(ActionEvent actionEvent) {
        stringCheck();
        if (button0.isSelected()) {
            tipSize.setText(String.valueOf(0));
        }
        if (button3.isSelected()) {
            tipSize.setText(String.valueOf((int) ((Double.parseDouble(inputCheckAmount.getText())) * 0.03)));
        }
        if (button5.isSelected()) {
            tipSize.setText(String.valueOf((int) ((Double.parseDouble(inputCheckAmount.getText())) * 0.05)));
        }
        if (button7.isSelected()) {
            tipSize.setText(String.valueOf((int) ((Double.parseDouble(inputCheckAmount.getText())) * 0.07)));
        }
        if (button10.isSelected()) {
            tipSize.setText(String.valueOf((int) ((Double.parseDouble(inputCheckAmount.getText())) * 0.1)));
        }
        if (button15.isSelected()) {
            tipSize.setText(String.valueOf((int) ((Double.parseDouble(inputCheckAmount.getText())) * 0.15)));
        }
    }

    /**
     * Метод для перевода суммы чека
     * и чаевых в рубли
     *
     * @param actionEvent событие
     */
    public void transfer(ActionEvent actionEvent) {
        if (!rate.getText().isEmpty()) {
            moneyRub.setText(String.valueOf(Double.parseDouble(inputCheckAmount.getText()) * Double.parseDouble(rate.getText())));
            tipRub.setText(String.valueOf(Integer.parseInt(tipSize.getText()) * Double.parseDouble(rate.getText())));
        } else {
            if (comboBoxCurrency.getValue().equals("₽")) {
                moneyRub.setText(String.valueOf(Double.parseDouble(inputCheckAmount.getText())));
                tipRub.setText(String.valueOf(Integer.parseInt(tipSize.getText())));
            }
            if (comboBoxCurrency.getValue().equals("$")) {
                moneyRub.setText(String.valueOf((Double.parseDouble(inputCheckAmount.getText())) * 64.73));
                tipRub.setText(String.valueOf(Integer.parseInt(tipSize.getText()) * 64.73));
            }
            if (comboBoxCurrency.getValue().equals("€")) {
                moneyRub.setText(String.valueOf(Double.parseDouble(inputCheckAmount.getText()) * 72.26));
                tipRub.setText(String.valueOf((Integer.parseInt(tipSize.getText()) * 72.26)));
            }
        }
    }
}